# There is no better way how to obtain the vimfiles folder location :/
# https://bugzilla.redhat.com/show_bug.cgi?id=844975
%global vimfiles  %{_datadir}/vim/vimfiles

%global appdata_dir %{_datadir}/appdata

Name: vim-fugitive
Version: 2.2
Release: 2%{?dist}
Summary: A Git wrapper so awesome, it should be illegal
Group: Applications/Editors
License: Vim
URL: http://www.vim.org/scripts/script.php?script_id=2975
Source0: https://github.com/tpope/vim-fugitive/archive/v2.2/%{name}-%{version}.tar.gz
# Plug-in AppData for Gnome Software.
# https://github.com/tpope/vim-fugitive/pull/638
Source1: vim-fugitive.metainfo.xml
Requires: vim-common
Requires(post): %{_bindir}/vim
Requires(postun): %{_bindir}/vim
BuildArch: noarch

%description
fugitive.vim may very well be the best Git wrapper of all time. Check out these
features:

View any blob, tree, commit, or tag in the repository with :Gedit (and :Gsplit,
:Gvsplit, :Gtabedit, ...). Edit a file in the index and write to it to stage
the changes. Use :Gdiff to bring up the staged version of the file side by side
with the working tree version and use Vim's diff handling capabilities to stage
a subset of the file's changes.

Bring up the output of git-status with :Gstatus. Press `-` to add/reset a
file's changes, or `p` to add/reset --patch. And guess what :Gcommit does!

:Gblame brings up an interactive vertical split with git-blame output. Press
enter on a line to reblame the file as it stood in that commit, or`o` to open
that commit in a split.

:Gmove does a git-mv on a file and simultaneously renames the buffer. :Gremove
does a git-rm on a file and simultaneously deletes the buffer.

Use :Ggrep to search the work tree (or any arbitrary commit) with git-grep,
skipping over that which is not tracked in the repository. :Glog loads all
previous revisions of a file into the quickfix list so you can iterate over
them and watch the file evolve!

:Gread is a variant of `git checkout -- filename` that operates on the buffer
rather than the filename.  This means you can use `u` to undo it and you never
get any warnings about the file changing outside Vim. :Gwrite writes to both
the work tree and index versions of a file, making it like git-add when called
from a work tree file and like git-checkout when called from the index or a
blob in history.

Add an indicator with the current branch in (surprise!) your statusline.

Oh, and of course there's :Git for running any arbitrary command.

%prep
%setup -q

%build


%install
mkdir -p %{buildroot}%{vimfiles}
cp -pr doc plugin %{buildroot}%{vimfiles}

# Install AppData.
mkdir -p %{buildroot}%{appdata_dir}
install -m 644 %{SOURCE1} %{buildroot}%{appdata_dir}

%post
vim -c ":helptags %{vimfiles}/doc" -c :q &> /dev/null

%postun
> %{vimfiles}/doc/tags
vim -c ":helptags %{vimfiles}/doc" -c :q &> /dev/null

%files
%doc CONTRIBUTING.markdown README.markdown
%{vimfiles}/doc/*
%{vimfiles}/plugin/*
%{appdata_dir}/vim-fugitive.metainfo.xml


%changelog
* Tue May 12 2015 Vít Ondruch <vondruch@redhat.com> - 2.2-2
- Remove something like RPM macro from description.

* Tue May 12 2015 Vít Ondruch <vondruch@redhat.com> - 2.2-1
- Initial package.
